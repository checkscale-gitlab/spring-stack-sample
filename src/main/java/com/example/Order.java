package com.example;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import java.time.Instant;
import org.springframework.lang.Nullable;

@Table("T_ORDER")
public class Order {

  @Id
  private Long id;
  
  @NotNull
  @Column("ORDER_DATE")
  @JsonProperty
  private Instant orderDate;

  @NotNull
  @Column("DELIVERY_DATE")
  @JsonProperty
  private Instant deliveryDate;

  @Nullable
  @Column("NOTES")
  @JsonProperty
  private String notes;

  @NotNull
  @Column("CUSTOMER_ID")
  @JsonProperty
  private Long customerId;

  public Long getId() {
    return id;
  }

  
  public Instant getOrderDate() { return orderDate; }

  public Instant getDeliveryDate() { return deliveryDate; }

  public String getNotes() { return notes; }

  public Long getCustomerId() { return customerId; }

  public void setId(Long id) {
    this.id = id;
  }

  
  public void setOrderDate(Instant orderDate) { this.orderDate = orderDate; }

  public void setDeliveryDate(Instant deliveryDate) { this.deliveryDate = deliveryDate; }

  public void setNotes(String notes) { this.notes = notes; }

  public void setCustomerId(Long customerId) { this.customerId = customerId; }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Order order = (Order) o;
    return Objects.equals(id, order.id)
        && Objects.equals(orderDate, order.orderDate)
        && Objects.equals(deliveryDate, order.deliveryDate)
        && Objects.equals(notes, order.notes)
        && Objects.equals(customerId, order.customerId);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("Order{");
    sb.append("id=").append(id);
    sb.append(", orderDate=").append(orderDate);
    sb.append(", deliveryDate=").append(deliveryDate);
    sb.append(", notes=").append(notes);
    sb.append(", customerId=").append(customerId);
    sb.append('}');
    return sb.toString();
  }

}
