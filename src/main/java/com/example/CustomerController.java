package com.example;

import static com.example.util.FluxUtils.wrapNotFoundIfEmpty;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/customers")
public class CustomerController {

  private final Logger logger = LoggerFactory.getLogger(getClass());
  private final CustomerRepository repository;
  private final OrderRepository ordersRepository;

  public CustomerController(CustomerRepository repository, OrderRepository ordersRepository) {
    this.repository = repository;
    this.ordersRepository = ordersRepository;
  }

  @GetMapping()
  public Flux<Customer> listAll() {
    logger.debug("listAll()");
    return repository.findAll();
  }

  @GetMapping("/{id}")
  public Mono<Customer> getOne(@PathVariable Long id) {
    logger.debug("getOne({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id), "customer", id);
  }

  @DeleteMapping("/{id}")
  public Mono<Customer> delete(@PathVariable Long id) {
    logger.debug("delete({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id)
        .flatMap(customer -> repository.delete(customer).thenReturn(customer)), "customer", id);
  }

  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<Customer> create(@RequestBody @Valid Customer customer) {
    logger.debug("create({})", customer);
    return repository.save(customer);
  }

  @PutMapping("/{id}")
  public Mono<Customer> update(@PathVariable Long id, @RequestBody @Valid Customer entity) {
    logger.debug("create({} : {})", id, entity);
    entity.setId(id);
    return wrapNotFoundIfEmpty(repository.save(entity), "customer", id);
  }
  
  
  @GetMapping("/{id}/orders")
  public Flux<Order> listAllOrders(@PathVariable Long id) {
    logger.debug("listAllOrders({})", id);
    return wrapNotFoundIfEmpty(getOne(id), "customer", id)
        .flatMapMany(customer -> ordersRepository.findByCustomerId(customer.getId()));
  }

  @GetMapping("/{id}/orders/{ordersId}")
  public Mono<Order> getOneOrders(@PathVariable Long id, @PathVariable Long ordersId) {
    logger.debug("getOneOrders({}, {})", id, ordersId);
    return wrapNotFoundIfEmpty(getOne(id), "customer", id)
        .flatMap(customer -> wrapNotFoundIfEmpty(ordersRepository.findById(ordersId), "orders", ordersId));
  }

  @DeleteMapping("/{id}/orders/{ordersId}")
  public Mono<Order> deleteOrders(@PathVariable Long id, @PathVariable Long ordersId) {
    logger.debug("deleteOrders({}, {})", id, ordersId);
    return wrapNotFoundIfEmpty(getOne(id), "customer", id)
        .flatMap(customer -> wrapNotFoundIfEmpty(ordersRepository.findById(ordersId), "orders", ordersId))
        .flatMap(orders -> ordersRepository.delete(orders).thenReturn(orders));
  }

  @PostMapping("/{id}/orders")
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<Order> createOrders(@PathVariable Long id, @RequestBody @Valid Order orders) {
    logger.debug("createOrders({}, {})", id, orders);
    return wrapNotFoundIfEmpty(getOne(id), "customer", id)
        .flatMap(customer -> {
          orders.setCustomerId(customer.getId());
          return ordersRepository.save(orders);
        });
  }

  @PutMapping("/{id}/orders/{ordersId}")
  public Mono<Order> updateOrders(@PathVariable Long id, @PathVariable Long ordersId, @RequestBody @Valid Order orders) {
    logger.debug("updateOrders({}, {}, {})", id, ordersId, orders);
    return wrapNotFoundIfEmpty(getOne(id), "customer", id)
        .flatMap(customer -> {
          orders.setId(ordersId);
          return wrapNotFoundIfEmpty(ordersRepository.save(orders), "orders", id);
        });
  }
  
}
