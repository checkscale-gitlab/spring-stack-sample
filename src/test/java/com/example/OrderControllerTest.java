package com.example;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.in;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

@ControllerTest
public class OrderControllerTest {

  @Autowired
  ApplicationContext context;

  @Autowired
  TestData data;

  @Autowired
  OrderRepository repository;

  WebTestClient rest;

  @BeforeEach
  public void setUp() {
    rest = WebTestClient.bindToApplicationContext(context)
        .configureClient()
        .responseTimeout(Duration.ofHours(1L))
        .build();
  }

  @Test
  public void createGetAndList() {

    Order entity = new Order();
    entity.setOrderDate(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setDeliveryDate(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setNotes("Malayan Tapir");
    entity.setCustomerId(data.getCustomer().getId());

    Order created = rest.post()
        .uri("/orders")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(Order.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getOrderDate());
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getDeliveryDate());
    assertEquals("Malayan Tapir", created.getNotes());
    assertEquals(data.getCustomer().getId(), created.getCustomerId());

    Order single = rest.get()
        .uri("/orders/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Order.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(single);
    assertEquals(created.getId(), single.getId());

    List<Order> list = rest.get()
        .uri("/orders")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(new ParameterizedTypeReference<List<Order>>() {
        })
        .returnResult()
        .getResponseBody();

    assertNotNull(list);
    assertThat(list, not(empty()));
    assertThat(created, is(in(list)));
  }

  @Test
  public void createUpdateAndGet() {

    Order entity = new Order();
    entity.setOrderDate(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setDeliveryDate(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setNotes("Malayan Tapir");
    entity.setCustomerId(data.getCustomer().getId());

    Order created = rest.post()
        .uri("/orders")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(Order.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getOrderDate());
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getDeliveryDate());
    assertEquals("Malayan Tapir", created.getNotes());
    assertEquals(data.getCustomer().getId(), created.getCustomerId());

    entity.setOrderDate(Instant.parse("2020-02-02T10:00:00.00Z"));
    entity.setDeliveryDate(Instant.parse("2020-02-02T10:00:00.00Z"));
    entity.setNotes("Malayan Tapir Updated");
    entity.setCustomerId(data.getCustomer().getId());

    Order updated = rest.put()
        .uri("/orders/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Order.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(updated);
    assertEquals(Instant.parse("2020-02-02T10:00:00.00Z"), updated.getOrderDate());
    assertEquals(Instant.parse("2020-02-02T10:00:00.00Z"), updated.getDeliveryDate());
    assertEquals("Malayan Tapir Updated", updated.getNotes());
    assertEquals(data.getCustomer().getId(), updated.getCustomerId());

    Order single = rest.get()
        .uri("/orders/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Order.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(single);
    assertEquals(created.getId(), single.getId());
  }

  @Test
  public void createDeleteAndGet() {

    Order entity = new Order();
    entity.setOrderDate(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setDeliveryDate(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setNotes("Malayan Tapir");
    entity.setCustomerId(data.getCustomer().getId());

    Order created = rest.post()
        .uri("/orders")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(Order.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getOrderDate());
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getDeliveryDate());
    assertEquals("Malayan Tapir", created.getNotes());
    assertEquals(data.getCustomer().getId(), created.getCustomerId());

    Order deleted = rest.delete()
        .uri("/orders/{id}", created.getId())
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Order.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(deleted);
    assertEquals(created.getId(), deleted.getId());

    rest.get()
        .uri("/orders/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }

  @Test
  public void getInvalid() {
    rest.get()
        .uri("/orders/{id}", "invalid")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isBadRequest();
  }

  @Test
  public void getMissing() {
    rest.get()
        .uri("/orders/{id}", -666L)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }
  
  @Test
  public void getCustomer() {
    Order entity = new Order();
    entity.setOrderDate(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setDeliveryDate(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setNotes("Malayan Tapir");
    entity.setCustomerId(data.getCustomer().getId());

    Order order = Objects.requireNonNull(repository.save(entity).block());

    Customer result = rest.get()
        .uri("/orders/{id}/customer", order.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Customer.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(result);
    assertEquals(result.getId(), result.getId());
  }

  @Test
  public void getInvalidCustomer() {
    rest.get()
        .uri("/orders/{id}/customer", "invalid")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isBadRequest();
  }

  @Test
  public void getMissingCustomer() {
    rest.get()
        .uri("/orders/{id}/customer", -666L)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }
  
  
  @Test
  public void createGetAndListItems() {
    OrderItem items = new OrderItem();
    items.setOrderId(data.getOrder().getId());
    items.setProductId(data.getProduct().getId());
    items.setAmount(17983);

    OrderItem created = rest.post()
        .uri("/orders/{orderId}/items", data.getOrder().getId())
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(items)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(OrderItem.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);

    OrderItem single = rest.get()
        .uri("/orders/{orderId}/items/{itemsId}", data.getOrder().getId(), created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(OrderItem.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(single);
    assertEquals(created.getId(), single.getId());

    List<OrderItem> list = rest.get()
        .uri("/orders/{orderId}/items", data.getOrder().getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(new ParameterizedTypeReference<List<OrderItem>>() {
        })
        .returnResult()
        .getResponseBody();

    assertNotNull(list);
    assertThat(list, not(empty()));
    assertThat(created, is(in(list)));
  }

  @Test
  public void createUpdateAndGetItems() {
    OrderItem items = new OrderItem();
    items.setOrderId(data.getOrder().getId());
    items.setProductId(data.getProduct().getId());
    items.setAmount(17983);

    OrderItem created = rest.post()
        .uri("/orders/{orderId}/items", data.getOrder().getId())
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(items)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(OrderItem.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals(data.getOrder().getId(), created.getOrderId());
    assertEquals(17983, created.getAmount());

    items.setAmount(18025);

    OrderItem updated = rest.put()
        .uri("/orders/{orderId}/items/{itemsId}", data.getOrder().getId(), created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(items)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(OrderItem.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(updated);
    assertEquals(data.getOrder().getId(), updated.getOrderId());
    assertEquals(18025, updated.getAmount());

    OrderItem single = rest.get()
        .uri("/orders/{orderId}/items/{itemsId}", data.getOrder().getId(), created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(OrderItem.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(single);
    assertEquals(created.getId(), single.getId());
  }

  @Test
  public void createDeleteAndGetItems() {
    OrderItem items = new OrderItem();
    items.setOrderId(data.getOrder().getId());
    items.setProductId(data.getProduct().getId());
    items.setAmount(17983);

    OrderItem created = rest.post()
        .uri("/orders/{orderId}/items", data.getOrder().getId())
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(items)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(OrderItem.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals(data.getOrder().getId(), created.getOrderId());
    assertEquals(17983, created.getAmount());

    OrderItem deleted = rest.delete()
        .uri("/orders/{orderId}/items/{itemsId}", data.getOrder().getId(), created.getId())
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(OrderItem.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(deleted);
    assertEquals(created.getId(), deleted.getId());

    rest.get()
        .uri("/orders/{orderId}/items/{itemsId}", data.getOrder().getId(), created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }

  @Test
  public void getInvalidItems() {
    Order entity = new Order();
    entity.setOrderDate(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setDeliveryDate(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setNotes("Malayan Tapir");
    entity.setCustomerId(data.getCustomer().getId());
    Order order = Objects.requireNonNull(repository.save(entity).block());

    rest.get()
        .uri("/orders/{orderId}/items/{itemsId}", data.getOrder().getId(), "invalid")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isBadRequest();
  }

  @Test
  public void getMissingItems() {
    Order entity = new Order();
    entity.setOrderDate(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setDeliveryDate(Instant.parse("2020-02-01T10:00:00.00Z"));
    entity.setNotes("Malayan Tapir");
    entity.setCustomerId(data.getCustomer().getId());
    Order order = Objects.requireNonNull(repository.save(entity).block());

    rest.get()
        .uri("/orders/{orderId}/items/{itemsId}", data.getOrder().getId(), -666L)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }
  
}
