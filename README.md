# spring-stack-sample project

This project uses Reactive Spring Framework.

If you want to learn more about Reactive Spring, please visit: https://spring.io/reactive .

## Model

```plantuml

class Customer {
  + String name
  + Order orders
}



class Order {
  + datetime orderDate
  + datetime deliveryDate
  + String notes?
  + Customer customer
  + OrderItem items
}
Order "*" *--o "1" Customer : customer


class Product {
  + String name
  + String code
  + ProductType type
  + OrderItem orders
}

Product --> ProductType : type

class OrderItem {
  + int amount
  + Order order
  + Product product
}
OrderItem "*" *--o "1" Order : order
OrderItem "*" *--o "1" Product : product


enum ProductType {
  - SIMPLE
  - COMPOUND
}

```

[//]: # "mastic-pin-readme"
